This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Description

this project is a demo to async library which takes error, content components & promise to fetch data from remotely.

## Available Scripts

In the project directory, you can run:

### `npm install`

installs all required packages, libraries or dependencies for running the app.

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
