import { combineReducers } from 'redux';
import loadingReducer from './loadingReducer';

// NOTE: current type definition of Reducer in 'redux-actions' module
// doesn't go well with redux@4
export const rootReducer = combineReducers({
  loading: loadingReducer
});