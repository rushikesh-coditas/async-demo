import React from 'react';
import AsyncImpl from "./Asynclibrary/Components/async";
import { get } from './Asynclibrary/Utils/HTTP';
import { reload } from './Asynclibrary/Actions/loadingActions';

export class App extends React.Component {

  getPromise = async () => {
    return get('https://api.github.com/search/users?q=chandan');
  }

  renderContent = (data) =>  data.items.map(i => <div>{i.login}</div>)

  render() {
    return(
      <div>
        <button onClick={() => reload("iten1")} >reload</button>
        <AsyncImpl
          promise={this.getPromise}
          loader={<div> loading </div>}
          content={this.renderContent}
          error={<div>error has occured </div>}
          identifier="iten1"
        />
      </div>
    )
  }
}
