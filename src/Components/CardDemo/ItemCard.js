import React from "react";
import './carddemo.scss';
import remove from '../remove.jfif'

const ItemCard = (props) => {
    return (
        props.isImageInclude === true ?
        <div className="left-section">
            {props.itemList.map(eachrow => {
                return (
                    <div className="item-card-right">
                        <text className="item-text">{eachrow.name}</text>
                        <img onClick={() => props.onCheckBoxCheck(eachrow)} src={remove} className="checkbox" />
                    </div>
                )
            })}
        </div>
        :
        <div className="left-section">
            {props.itemList.map(eachrow => {
                return (
                    <div className="item-card">
                        <input checked={eachrow.check} onClick={() => props.onCheckBoxCheck(eachrow)} type="checkbox" className="checkbox" />
                        <text className="item-text">{eachrow.name}</text>
                    </div>
                )
            })}
        </div>
    )
}

export default ItemCard;