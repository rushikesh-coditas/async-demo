import React from "react";
import './carddemo.scss';
import ItemCard from './ItemCard';

class ArrayDemo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            arr1: [{ name: "Item1", check: false }, { name: "Item2", check: false }, { name: "Item3", check: false }, { name: "Item4", check: false }, { name: "Item5", check: false }],
            arr2: []
        }
    }

    onCheckBoxCheck = (eachrow) => {
        let arr = this.state.arr2;
        let remove = this.state.arr1;
        arr.push(eachrow);
        remove.splice(remove.indexOf(eachrow), 1);
        this.setState({ arr2: arr, arr1: remove });

    }

    removeItem = (eachrow) => {
        let arr = this.state.arr1;
        let remove = this.state.arr2;
        arr.push(eachrow);
        remove.splice(remove.indexOf(eachrow), 1);
        this.setState({ arr1: arr, arr2:remove });
    }

    render() {
        return (
            <section className="section">
                <div className="card">
                    <ItemCard itemList={this.state.arr1} onCheckBoxCheck={this.onCheckBoxCheck} />
                    <div className="seperator-line" />
                    <ItemCard itemList={this.state.arr2} onCheckBoxCheck={this.removeItem} isImageInclude={true} />
                </div>
            </section>
        )
    }
}

export default ArrayDemo;